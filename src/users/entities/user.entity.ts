import { Buystockorder } from 'src/buystock-orders/entities/buystock-order.entity';
import { Checkstock } from 'src/checkstock/entities/checkstock.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Usestockorder } from 'src/usestock-orders/entities/usestock-order.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({
    default: '',
  })
  name: string;

  @Column()
  gender: string;

  @Column({
    default: 'noimage.jpg',
  })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToOne(() => Employee)
  @JoinColumn({ name: 'empId' })
  employee: Employee;

  @ManyToOne(() => Role, (role) => role.users)
  role: Role;

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @OneToMany(() => Buystockorder, (buystockorders) => buystockorders.user)
  buystockorders: Buystockorder[];

  @OneToMany(() => Usestockorder, (usestockorders) => usestockorders.user)
  usestockorders: Usestockorder[];

  @OneToMany(() => Checkstock, (Checkstocks) => Checkstocks.user)
  Checkstocks: Checkstock[];

  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];
}
