/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class SalariesService {
  constructor(
    @InjectRepository(Salary) private salariesRepository: Repository<Salary>,
    // @InjectRepository(Employee) private employeesRepository: Repository<Employee>
  ) {}
  create(createSalaryDto: CreateSalaryDto): Promise<Salary> {
    return this.salariesRepository.save(createSalaryDto);
  }

  findAll(): Promise<Salary[]> {
    return this.salariesRepository
      .find
      // {relations: {
      //   employees:true,
      // },}
      ();
  }

  findOne(id: number) {
    return this.salariesRepository.findOne({
      where: { id },
      // relations: {
      //   employees:true,
      // },
    });
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    // const updateSalary = await this.employeesRepository.findOneOrFail({
    //   where: { id },
    // });
    await this.salariesRepository.update(id, {
      // ...updateSalary,
      ...updateSalaryDto,
    });
    const result = await this.salariesRepository.findOneOrFail({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteSalary = await this.salariesRepository.findOneOrFail({
      where: { id },
    });
    return await this.salariesRepository.remove(deleteSalary);
  }
}
