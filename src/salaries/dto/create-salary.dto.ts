/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';
import { Entity } from 'typeorm';
@Entity()
export class CreateSalaryDto {
  @IsNotEmpty()
  pay_date: string;

  @IsNotEmpty()
  emp_id: number;

  @IsNotEmpty()
  emp_name: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  pay_period: string;

  @IsNotEmpty()
  pay: number;

  @IsNotEmpty()
  work_hour: number;
}
