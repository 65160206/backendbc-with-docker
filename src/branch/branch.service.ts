import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BranchService {
  constructor(
    @InjectRepository(Branch)
    private branchsRepository: Repository<Branch>,
  ) {}

  create(createBranchDto: CreateBranchDto): Promise<Branch> {
    const branch = new Branch();
    branch.address = createBranchDto.address;
    branch.maneger = createBranchDto.maneger;
    branch.name = createBranchDto.name;
    branch.tels = createBranchDto.tels;
    return this.branchsRepository.save(branch);
  }

  findAll(): Promise<Branch[]> {
    return this.branchsRepository.find();
  }

  findOne(id: number) {
    return this.branchsRepository.findOneBy({ id: id });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const branch = await this.branchsRepository.findOneOrFail({
      where: { id },
    });
    branch.address = updateBranchDto.address;
    branch.maneger = updateBranchDto.maneger;
    branch.name = updateBranchDto.name;
    branch.tels = updateBranchDto.tels;
    this.branchsRepository.save(branch);
    const result = await this.branchsRepository.findOne({
      where: { id },
      relations: { stock: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteBranch = await this.branchsRepository.findOneBy({ id });
    return this.branchsRepository.remove(deleteBranch);
  }
}
