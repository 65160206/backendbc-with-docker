import { PartialType } from '@nestjs/swagger';
import { CreateCheckstockDto } from './create-checkstock.dto';

export class UpdateCheckstockDto extends PartialType(CreateCheckstockDto) {}
