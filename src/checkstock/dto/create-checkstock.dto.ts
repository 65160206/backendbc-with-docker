import { CheckstockItem } from '../entities/checkstockitem.entity';

export class CreateCheckstockDto {
  checkstockItems: CheckstockItem[];
}
