import {
  Controller,
  Get,
  Post,
  Body,
  // Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckstocksService } from './checkstock.service';
import { CreateCheckstockDto } from './dto/create-checkstock.dto';
// import { UpdateCheckstockDto } from './dto/update-checkstock.dto';

@Controller('checkstock')
export class CheckstocksController {
  constructor(private readonly checkstocksService: CheckstocksService) {}

  @Post()
  create(@Body() createCheckstockDto: CreateCheckstockDto) {
    return this.checkstocksService.create(createCheckstockDto);
  }

  @Get()
  findAll() {
    return this.checkstocksService.findAll();
  }
  @Get('/CheckStockItems')
  findAllCheckStockItems() {
    return this.checkstocksService.findAllCheckStockItems();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkstocksService.findOne(+id);
  }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateCheckstockDto: UpdateCheckstockDto,
  // ) {
  //   return this.checkstocksService.update(+id, updateCheckstockDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkstocksService.remove(+id);
  }
}
