import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Checkstock } from './checkstock.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Entity()
export class CheckstockItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  min: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column()
  status: string;

  @ManyToOne(
    () => Checkstock,
    (checkstockorders) => checkstockorders.checkstockItems,
    {
      onDelete: 'CASCADE',
    },
  )
  checkstock: Checkstock;

  @ManyToOne(() => Stock, (stock) => stock.checkstockItems)
  stock: Stock;
}
