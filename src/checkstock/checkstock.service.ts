import { Injectable } from '@nestjs/common';
import { CreateCheckstockDto } from './dto/create-checkstock.dto';
// import { UpdateCheckstockDto } from './dto/update-checkstock.dto';
import { Checkstock } from './entities/checkstock.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckstockItem } from './entities/checkstockitem.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Injectable()
export class CheckstocksService {
  constructor(
    @InjectRepository(Checkstock)
    private checkstockRepository: Repository<Checkstock>,
    @InjectRepository(CheckstockItem)
    private checkstockItemsRepository: Repository<CheckstockItem>,
    @InjectRepository(Stock)
    private checkstocksRepository: Repository<Stock>,
  ) {}
  // async create(createCheckstockDto: CreateCheckstockDto) {
  //   const checkstock = new Checkstock();
  //   checkstock.checkstockItems = [];
  //   console.log(createCheckstockDto.checkstockItems);
  //   for (const oi of createCheckstockDto.checkstockItems) {
  //     const checkstockItem = new CheckstockItem();
  //     checkstockItem.stock = await this.checkstocksRepository.findOneBy({
  //       id: oi.stockId,
  //     });
  //     checkstockItem.name = checkstockItem.stock.name;
  //     checkstockItem.balance = checkstockItem.stock.balance;
  //     checkstockItem.min = checkstockItem.stock.min;
  //     checkstockItem.status = checkstockItem.stock.status;
  //     checkstockItem.unit = checkstockItem.stock.unit;
  //     await this.checkstockItemsRepository.save(checkstockItem);
  //     checkstock.checkstockItems.push(checkstockItem);
  //   }
  //   return this.checkstockRepository.save(checkstock);
  // }

  async create(createCheckstockDto: CreateCheckstockDto) {
    console.log(createCheckstockDto.checkstockItems);
    for (const ci of createCheckstockDto.checkstockItems) {
      let checkstockItem = new CheckstockItem();
      checkstockItem = ci;
      await this.checkstockItemsRepository.save(checkstockItem);
    }

    return this.checkstockRepository.save(createCheckstockDto);
  }

  findAll() {
    return this.checkstockRepository.find({
      relations: { checkstockItems: true, user: true },
      order: {
        id: 'DESC',
      },
    });
  }

  findOne(id: number) {
    return this.checkstockRepository.findOneOrFail({
      where: { id },
      relations: { checkstockItems: true, user: true },
    });
  }

  // update(id: number, UpdateCheckstockDto: UpdateCheckstockDto) {
  //   return `This action updates a #${id} `;
  // }

  async remove(id: number) {
    const deleteOrder = await this.checkstockRepository.findOneOrFail({
      where: { id },
    });
    await this.checkstockRepository.remove(deleteOrder);

    return deleteOrder;
  }

  findAllCheckStockItems() {
    return this.checkstockItemsRepository.find({
      relations: { checkstock: true },
    });
  }
}
