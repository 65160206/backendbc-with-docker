import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptItem } from './receiptItem.entity';
import { Member } from 'src/members/entities/member.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @Column()
  totalBefore: number;

  @Column()
  memberDiscount: number;

  @Column()
  promotionDiscount: number;

  @Column()
  totalDiscount: number;

  @Column()
  total: number;

  @Column()
  receivedAmount: number;

  @Column()
  cashBack: number;

  @Column()
  totalQty: number;

  @Column()
  paymentType: string;

  @ManyToOne(() => User, (user) => user.receipts, { onDelete: 'CASCADE' })
  user: User;

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.receipt, {
    onDelete: 'CASCADE',
  })
  receiptItems: ReceiptItem[];

  @ManyToOne(() => Member, (member) => member.receipts, {
    onDelete: 'CASCADE',
    nullable: true,
  })
  member: Member;

  @ManyToOne(() => Branch, (branch) => branch.receipts, {
    onDelete: 'CASCADE',
    nullable: true,
  })
  branch: Branch;

  @ManyToMany(() => Promotion, (promotion) => promotion.receipts, {
    onDelete: 'CASCADE',
    nullable: true,
  })
  @JoinTable()
  promotions: Promotion[];
}
