/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Connection, Repository } from 'typeorm';
import { ReceiptItem } from './entities/receiptItem.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptsRepository: Repository<Receipt>,
    @InjectRepository(ReceiptItem)
    private receiptItemsRepository: Repository<ReceiptItem>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto) {
    console.log(createReceiptDto);
    for (const ri of createReceiptDto.receiptItems) {
      let receiptItem = new ReceiptItem();
      receiptItem = ri;
      await this.receiptItemsRepository.save(receiptItem);
    }

    return this.receiptsRepository.save(createReceiptDto);
  }
  findAll() {
    return this.receiptsRepository.find({
      relations: {
        receiptItems: true,
        user: true,
        member: true,
        promotions: true,
        branch: true,
      },
    });
  }
  async findAllsp() {
    const results = await this.receiptsRepository.query('CALL getBestSeller()');
    return results[0];
  }

  async findAllspp(a: string, b: string) {
    const results = await this.receiptsRepository.query(
      `CALL viewOrderAvg('${a}','${b}')`,
    );
    return results[0];
  }

  findOne(id: number) {
    return this.receiptsRepository.findOneOrFail({
      where: { id },
      relations: { receiptItems: true, user: true, member: true },
    });
  }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    for (const ri of updateReceiptDto.receiptItems) {
      let receiptItem = new ReceiptItem();
      receiptItem = ri;
      await this.receiptItemsRepository.save(receiptItem);
    }

    return this.receiptsRepository.save(updateReceiptDto);
  }

  async remove(id: number) {
    const deleteReceipt = await this.receiptsRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptsRepository.remove(deleteReceipt);

    return deleteReceipt;
  }

  async findViewOrderAvg() {
    const results = await this.receiptsRepository.query(
      'SELECT * FROM `viewOrderAvg`',
    );
    return results;
  }

  async findReceiptInRangeDate(
    type: string,
    startDate: string,
    endDate: string,
  ): Promise<any> {
    try {
      // Execute the SET statement to set @startDate
      await this.receiptsRepository.query(
        `SET @startDate = STR_TO_DATE('${startDate}', '%d-%m-%Y')`,
      );

      // Execute the SET statement to set @endDate
      await this.receiptsRepository.query(
        `SET @endDate = STR_TO_DATE('${endDate}', '%d-%m-%Y')`,
      );

      // Execute the stored procedure GetReceiptsByDateRange
      const [results] = await this.receiptsRepository.query(`
        CALL GetReceiptsByDateRange('${type}', @startDate, @endDate)
      `);

      return results;
    } catch (error) {
      console.error('Error occurred:', error);
      throw error; // Rethrow the error for handling by the caller
    }
  }
}
