/* eslint-disable prettier/prettier */
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Attendance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  employeeId: number;

  @Column()
  name: string;

  @Column({ type: 'date' })
  date: Date;

  @Column()
  clockIn: string;

  @Column()
  clockOut: string;

  @Column()
  status: string;

  @Column({ default: 0 })
  workedTime: number;

  @ManyToOne(() => Employee, (employee) => employee.attendances)
  @JoinColumn({ name: 'employeeId' })
  employee: Employee;
}
