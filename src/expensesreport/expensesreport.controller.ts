import { Controller, Get } from '@nestjs/common';
import { ExpensesreportService } from './expensesreport.service';

@Controller('expensesreport')
export class ExpensesreportController {
  constructor(private expensesreportService: ExpensesreportService) {}
  @Get('reportexpenses1')
  reportexpenses1() {
    return this.expensesreportService.reportexpenses1;
  }
}
