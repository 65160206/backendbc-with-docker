import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class ExpensesreportService {
  constructor(private dataSource: DataSource) {}

  reportexpenses1() {
    return this.dataSource.query(
      `SELECT * FROM store_expenses WHERE branchId = 1;`,
    );
  }
}
