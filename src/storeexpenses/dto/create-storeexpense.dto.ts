export class CreateStoreexpenseDto {
  status: string;
  electricBill: string;
  waterBill: string;
  total: string;
  due_date: string;
  branch: string;
}
