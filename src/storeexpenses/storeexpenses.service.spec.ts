import { Test, TestingModule } from '@nestjs/testing';
import { StoreexpensesService } from './storeexpenses.service';

describe('StoreexpensesService', () => {
  let service: StoreexpensesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StoreexpensesService],
    }).compile();

    service = module.get<StoreexpensesService>(StoreexpensesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
