import { Injectable } from '@nestjs/common';
import { CreateStoreexpenseDto } from './dto/create-storeexpense.dto';
import { UpdateStoreexpenseDto } from './dto/update-storeexpense.dto';
import { StoreExpenses } from './entities/storeexpense.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StoreexpensesService {
  constructor(
    @InjectRepository(StoreExpenses)
    private storeexpensesRepository: Repository<StoreExpenses>,
  ) {}
  create(createStoreexpenseDto: CreateStoreexpenseDto) {
    const storeexpenses = new StoreExpenses();
    storeexpenses.status = createStoreexpenseDto.status;
    storeexpenses.electricBill = parseInt(createStoreexpenseDto.electricBill);
    storeexpenses.waterBill = parseInt(createStoreexpenseDto.waterBill);
    storeexpenses.total = storeexpenses.electricBill + storeexpenses.waterBill;
    storeexpenses.due_date = createStoreexpenseDto.due_date;
    storeexpenses.branch =
      typeof createStoreexpenseDto.branch === 'string'
        ? JSON.parse(createStoreexpenseDto.branch)
        : createStoreexpenseDto.branch;
    return this.storeexpensesRepository.save(storeexpenses);
  }

  findAll() {
    return this.storeexpensesRepository.find({
      relations: { branch: true },
    });
  }

  findOne(id: number) {
    return this.storeexpensesRepository.findOne({
      where: { id },
      relations: { branch: true },
    });
  }

  async update(id: number, updateStoreexpenseDto: UpdateStoreexpenseDto) {
    const storeexpenses = await this.storeexpensesRepository.findOneOrFail({
      where: { id },
      relations: { branch: true },
    });

    storeexpenses.status = updateStoreexpenseDto.status;
    storeexpenses.electricBill = parseInt(updateStoreexpenseDto.electricBill);
    storeexpenses.waterBill = parseInt(updateStoreexpenseDto.waterBill);
    storeexpenses.total = storeexpenses.electricBill + storeexpenses.waterBill;
    storeexpenses.due_date = updateStoreexpenseDto.due_date;
    storeexpenses.branch =
      typeof updateStoreexpenseDto.branch === 'string'
        ? JSON.parse(updateStoreexpenseDto.branch)
        : updateStoreexpenseDto.branch;
    await this.storeexpensesRepository.save(storeexpenses);
    const updateStoreexpense = await this.storeexpensesRepository.findOne({
      where: { id },
      relations: { branch: true },
    });

    return updateStoreexpense;
  }

  async remove(id: number) {
    const deleteStoreExpenses =
      await this.storeexpensesRepository.findOneOrFail({
        where: { id },
      });
    await this.storeexpensesRepository.remove(deleteStoreExpenses);

    return deleteStoreExpenses;
  }

  async reportexpenses1() {
    const results = await this.storeexpensesRepository.query(
      'SELECT * FROM `reportexpenses1`',
    );
    return results;
  }
}
