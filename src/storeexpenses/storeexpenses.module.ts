import { Module } from '@nestjs/common';
import { StoreexpensesService } from './storeexpenses.service';
import { StoreexpensesController } from './storeexpenses.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoreExpenses } from './entities/storeexpense.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StoreExpenses])],
  controllers: [StoreexpensesController],
  providers: [StoreexpensesService],
})
export class StoreexpensesModule {}
