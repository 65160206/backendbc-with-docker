/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private StocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    stock.name = createStockDto.name;
    stock.price = parseInt(createStockDto.price);
    stock.min = parseInt(createStockDto.min);
    stock.balance = parseInt(createStockDto.balance);
    stock.unit = createStockDto.unit;
    stock.status = createStockDto.status;
    stock.branch = JSON.parse(createStockDto.branch);
    if (createStockDto.image && createStockDto.image !== '') {
      stock.image = createStockDto.image;
    }
    return this.StocksRepository.save(stock);
  }
  findAll() {
    return this.StocksRepository.find({
      relations: { branch: true },
    });
  }
  async findAllspp(status: string, unit: string) {
    const callQueryString = `
      CALL GetMaterialInfo('${status}', '${unit}');
    `;

    console.log(callQueryString);

    // Execute CALL statement
    await this.StocksRepository.query(callQueryString);

    // Execute SELECT statement
    const results = await this.StocksRepository.query(callQueryString);

    return results[0];
  }

  findOne(id: number) {
    return this.StocksRepository.findOneOrFail({
      where: { id },
      relations: { branch: true },
    });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.StocksRepository.findOneOrFail({
      where: { id },
    });
    stock.name = updateStockDto.name;
    stock.price = parseFloat(updateStockDto.price);
    stock.min = parseFloat(updateStockDto.min);
    stock.balance = parseFloat(updateStockDto.balance);
    stock.unit = updateStockDto.unit;
    stock.status = updateStockDto.status;
    stock.branch = JSON.parse(updateStockDto.branch);
    if (updateStockDto.image && updateStockDto.image !== '') {
      stock.image = updateStockDto.image;
    }
    this.StocksRepository.save(stock);
    const result = await this.StocksRepository.findOne({
      where: { id },
      relations: { branch: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteStock = await this.StocksRepository.findOneOrFail({
      where: { id },
    });
    await this.StocksRepository.remove(deleteStock);
    return deleteStock;
  }
}
