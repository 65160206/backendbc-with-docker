import { Branch } from 'src/branch/entities/branch.entity';
import { BuystockorderItem } from 'src/buystock-orders/entities/buystock-orderitem.entity';
import { CheckstockItem } from 'src/checkstock/entities/checkstockitem.entity';
import { UsestockorderItem } from 'src/usestock-orders/entities/usestock-orderitem.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ default: 0 })
  price: number;

  @Column()
  min: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column({
    default: 'Available',
  })
  status: string;

  @ManyToOne(() => Branch, (branch) => branch.stock)
  branch: Branch;

  @OneToMany(
    () => BuystockorderItem,
    (buystockorderItems) => buystockorderItems.stock,
  )
  buystockorderItems: BuystockorderItem[];

  @OneToMany(
    () => UsestockorderItem,
    (usestockorderItems) => usestockorderItems.stock,
  )
  usestockorderItems: UsestockorderItem[];

  @OneToMany(() => CheckstockItem, (checkstockItems) => checkstockItems.stock)
  checkstockItems: CheckstockItem[];
}
