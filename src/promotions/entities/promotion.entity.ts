import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PromotionItem } from './promotionItem.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  code: string;

  @Column()
  name: string;

  @Column()
  startdate: string;

  @Column()
  enddate: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(
    () => PromotionItem,
    (promotionItem) => promotionItem.promotions,
    {
      cascade: true,
    },
  )
  @JoinTable()
  promotionItems?: PromotionItem[];

  @ManyToMany(() => Receipt, (receipt) => receipt.promotions, {
    onDelete: 'CASCADE',
    nullable: true,
  })
  receipts: Receipt[];
}
