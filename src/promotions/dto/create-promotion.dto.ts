import { PromotionItem } from '../entities/promotionItem.entity';

export class CreatePromotionDto {
  name: string;

  code: string;

  startdate: string;

  enddate: string;

  promotionItems?: PromotionItem[];
}
