import { Module } from '@nestjs/common';
import { PromotionsService } from './promotions.service';
import { PromotionsController } from './promotions.controller';
import { Promotion } from './entities/promotion.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PromotionItem } from './entities/promotionItem.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion, PromotionItem])],
  controllers: [PromotionsController],
  providers: [PromotionsService],
})
export class PromotionsModule {}
