import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { MembersModule } from './members/members.module';
import { Member } from './members/entities/member.entity';
import { Branch } from './branch/entities/branch.entity';
import { BranchModule } from './branch/branch.module';
import { PromotionsModule } from './promotions/promotions.module';
import { Promotion } from './promotions/entities/promotion.entity';
import { PromotionItem } from './promotions/entities/promotionItem.entity';
import { Stock } from './stocks/entities/stock.entity';
import { StocksModule } from './stocks/stocks.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { Order } from './orders/entities/order.entity';
import { ProductsModule } from './products/products.module';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { AuthModule } from './auth/auth.module';
import { OrdersModule } from './orders/orders.module';
import { BuystockOrdersModule } from './buystock-orders/buystock-orders.module';
import { Buystockorder } from './buystock-orders/entities/buystock-order.entity';
import { BuystockorderItem } from './buystock-orders/entities/buystock-orderitem.entity';
import { EmployeesModule } from './employees/employees.module';
import { StatusModule } from './status/status.module';
import { AttendanceModule } from './attendance/attendance.module';
import { Employee } from './employees/entities/employee.entity';
import { Attendance } from './attendance/entities/attendance.entity';
import { Status } from './status/entities/status.entity';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { ReceiptItem } from './receipts/entities/receiptItem.entity';
import { StoreexpensesModule } from './storeexpenses/storeexpenses.module';
import { StoreExpenses } from './storeexpenses/entities/storeexpense.entity';
import { UsestockOrdersModule } from './usestock-orders/usestock-orders.module';
import { Usestockorder } from './usestock-orders/entities/usestock-order.entity';
import { UsestockorderItem } from './usestock-orders/entities/usestock-orderitem.entity';
// import { CheckstockModule } from './checkstock/checkstock.module';
// import { Checkstock } from './checkstock/entities/checkstock.entity';
// import { Checkstockitem } from './checkstock/entities/checkstockitem';
import { Checkstock } from './checkstock/entities/checkstock.entity';
import { CheckstockItem } from './checkstock/entities/checkstockitem.entity';
import { CheckstocksModule } from './checkstock/checkstock.module';
import { Salary } from './salaries/entities/salary.entity';
import { SalariesModule } from './salaries/salaries.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'MYSQL_ROOT_PASSWORD',
      database: 'webpro',
      entities: [
        Attendance,
        User,
        Role,
        OrderItem,
        Type,
        Product,
        Branch,
        Stock,
        Buystockorder,
        BuystockorderItem,
        Member,
        Order,
        Employee,
        Salary,
        Promotion,
        PromotionItem,
        Attendance,
        Status,
        Receipt,
        ReceiptItem,
        StoreExpenses,
        Usestockorder,
        UsestockorderItem,
        Checkstock,
        CheckstockItem,
      ],
      synchronize: true,
    }),

    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    AttendanceModule,
    RolesModule,
    TypesModule,
    ProductsModule,
    TemperatureModule,
    UsersModule,
    MembersModule,
    BranchModule,
    StocksModule,
    PromotionsModule,
    OrdersModule,
    AuthModule,
    BuystockOrdersModule,
    EmployeesModule,
    SalariesModule,
    StatusModule,
    AttendanceModule,
    ReceiptsModule,
    StoreexpensesModule,
    UsestockOrdersModule,
    CheckstocksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
