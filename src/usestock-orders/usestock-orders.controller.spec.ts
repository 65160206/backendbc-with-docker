import { Test, TestingModule } from '@nestjs/testing';
import { UsestockOrdersController } from './usestock-orders.controller';
import { UsestockOrdersService } from './usestock-orders.service';

describe('UsestockOrdersController', () => {
  let controller: UsestockOrdersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsestockOrdersController],
      providers: [UsestockOrdersService],
    }).compile();

    controller = module.get<UsestockOrdersController>(UsestockOrdersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
