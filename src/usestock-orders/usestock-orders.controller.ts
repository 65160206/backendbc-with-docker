import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UsestockOrdersService } from './usestock-orders.service';
import { CreateUsestockOrderDto } from './dto/create-usestock-order.dto';
import { UpdateUsestockOrderDto } from './dto/update-usestock-order.dto';

@Controller('usestock-orders')
export class UsestockOrdersController {
  constructor(private readonly usestockOrdersService: UsestockOrdersService) {}

  @Post()
  create(@Body() createUsestockOrderDto: CreateUsestockOrderDto) {
    return this.usestockOrdersService.create(createUsestockOrderDto);
  }

  @Get()
  findAll() {
    return this.usestockOrdersService.findAll();
  }

  @Get('getUseStockGraph1')
  findViewOrderUseStockMM() {
    return this.usestockOrdersService.findViewOrderUseStockMM();
  }

  @Get('getUseStockGraph2')
  findViewOrderUseStockYY() {
    return this.usestockOrdersService.findViewOrderUseStockYY();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usestockOrdersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateUsestockOrderDto: UpdateUsestockOrderDto,
  ) {
    return this.usestockOrdersService.update(+id, updateUsestockOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usestockOrdersService.remove(+id);
  }
}
