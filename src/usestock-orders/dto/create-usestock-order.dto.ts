export class CreateUsestockOrderDto {
  usestockOrderItems: {
    productId: number;
    qty: number;
  }[];
  userId: number;
}
