import { PartialType } from '@nestjs/swagger';
import { CreateUsestockOrderDto } from './create-usestock-order.dto';

export class UpdateUsestockOrderDto extends PartialType(
  CreateUsestockOrderDto,
) {}
