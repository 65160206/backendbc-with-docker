import { Module } from '@nestjs/common';
import { UsestockOrdersService } from './usestock-orders.service';
import { UsestockOrdersController } from './usestock-orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usestockorder } from './entities/usestock-order.entity';
import { UsestockorderItem } from './entities/usestock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Usestockorder, UsestockorderItem, User, Stock]),
  ],
  controllers: [UsestockOrdersController],
  providers: [UsestockOrdersService],
})
export class UsestockOrdersModule {}
