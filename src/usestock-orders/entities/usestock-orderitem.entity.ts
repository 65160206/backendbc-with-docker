import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Usestockorder } from './usestock-order.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Entity()
export class UsestockorderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @Column()
  unit: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => Usestockorder,
    (usestockorders) => usestockorders.usestockorderItems,
    {
      onDelete: 'CASCADE',
    },
  )
  usestockorders: Usestockorder;

  @ManyToOne(() => Stock, (stock) => stock.usestockorderItems)
  stock: Stock;
}
