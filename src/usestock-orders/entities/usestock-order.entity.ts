import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UsestockorderItem } from './usestock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Usestockorder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => UsestockorderItem,
    (usestockorderItems) => usestockorderItems.usestockorders,
  )
  usestockorderItems: UsestockorderItem[];

  @ManyToOne(() => User, (user) => user.usestockorders)
  user: User;
}
