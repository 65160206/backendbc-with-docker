import { Injectable } from '@nestjs/common';
import { CreateUsestockOrderDto } from './dto/create-usestock-order.dto';
import { UpdateUsestockOrderDto } from './dto/update-usestock-order.dto';
import { Usestockorder } from './entities/usestock-order.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UsestockorderItem } from './entities/usestock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Injectable()
export class UsestockOrdersService {
  constructor(
    @InjectRepository(Usestockorder)
    private usestockordersRepository: Repository<Usestockorder>,
    @InjectRepository(UsestockorderItem)
    private usestockorderItemsRepository: Repository<UsestockorderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Stock)
    private usestocksRepository: Repository<Stock>,
  ) {}
  async create(createUsestockOrderDto: CreateUsestockOrderDto) {
    const usestockOrder = new Usestockorder();
    const user = await this.usersRepository.findOneBy({
      id: createUsestockOrderDto.userId,
    });
    usestockOrder.user = user;
    usestockOrder.total = 0;
    usestockOrder.qty = 0;
    usestockOrder.usestockorderItems = [];
    for (const oi of createUsestockOrderDto.usestockOrderItems) {
      const usestockOrderItem = new UsestockorderItem();
      usestockOrderItem.stock = await this.usestocksRepository.findOneBy({
        id: oi.productId,
      });
      const stock = usestockOrderItem.stock; //
      if (stock.balance - oi.qty >= 0) {
        stock.balance = stock.balance - oi.qty;
      } else {
        stock.balance = 0;
      }
      usestockOrderItem.name = usestockOrderItem.stock.name;
      usestockOrderItem.price = usestockOrderItem.stock.price;
      usestockOrderItem.qty = oi.qty;
      usestockOrderItem.unit = usestockOrderItem.stock.unit;
      usestockOrderItem.total = usestockOrderItem.price * usestockOrderItem.qty;
      await this.usestockorderItemsRepository.save(usestockOrderItem);
      usestockOrder.usestockorderItems.push(usestockOrderItem);
      usestockOrder.total += usestockOrderItem.total;
      usestockOrder.qty += usestockOrderItem.qty;
      this.usestocksRepository.save(stock); //
    }
    return this.usestockordersRepository.save(usestockOrder);
  }

  findAll() {
    return this.usestockordersRepository.find({
      relations: { usestockorderItems: true, user: true },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.usestockordersRepository.findOneOrFail({
      where: { id },
      relations: { usestockorderItems: true, user: true },
    });
  }

  update(id: number, UpdateUsestockOrderDto: UpdateUsestockOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.usestockordersRepository.findOneOrFail({
      where: { id },
    });
    await this.usestockordersRepository.remove(deleteOrder);

    return deleteOrder;
  }

  async findViewOrderUseStockMM() {
    const results = await this.usestocksRepository.query(
      'SELECT * FROM `viewUseStockGraph1`',
    );
    return results;
  }

  async findViewOrderUseStockYY() {
    const results = await this.usestocksRepository.query(
      'SELECT * FROM `viewUseStockGraph2`',
    );
    return results;
  }
}
