import { Attendance } from 'src/attendance/entities/attendance.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Status } from 'src/status/entities/status.entity';
import { StoreExpenses } from 'src/storeexpenses/entities/storeexpense.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  salary: number;

  @Column()
  dateIn: string;

  @Column({
    default: 'noimage.jpg',
  })
  image: string;

  @ManyToOne(() => Status, (status) => status.employees)
  status: Status[];

  @ManyToOne(() => Branch, (branch) => branch.employees)
  branch: Branch[];

  @OneToMany(() => Attendance, (attendance) => attendance.employee)
  attendances: Attendance[];

  // @OneToMany(() => Salary, (salary) => salary.employee)
  // salaries: Salary[];
}
