import { Module } from '@nestjs/common';
import { BuystockOrdersService } from './buystock-orders.service';
import { BuystockOrdersController } from './buystock-orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Buystockorder } from './entities/buystock-order.entity';
import { BuystockorderItem } from './entities/buystock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Buystockorder, BuystockorderItem, User, Stock]),
  ],
  controllers: [BuystockOrdersController],
  providers: [BuystockOrdersService],
})
export class BuystockOrdersModule {}
