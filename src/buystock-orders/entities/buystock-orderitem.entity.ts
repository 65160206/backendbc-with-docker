import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Buystockorder } from './buystock-order.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Entity()
export class BuystockorderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @Column()
  unit: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => Buystockorder,
    (buystockorders) => buystockorders.buystockorderItems,
    {
      onDelete: 'CASCADE',
    },
  )
  buystockorders: Buystockorder;

  @ManyToOne(() => Stock, (stock) => stock.buystockorderItems, {
    onDelete: 'CASCADE',
  })
  stock: Stock;
}
