import { Injectable } from '@nestjs/common';
import { CreateBuystockOrderDto } from './dto/create-buystock-order.dto';
import { UpdateBuystockOrderDto } from './dto/update-buystock-order.dto';
import { Buystockorder } from './entities/buystock-order.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BuystockorderItem } from './entities/buystock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Injectable()
export class BuystockOrdersService {
  constructor(
    @InjectRepository(Buystockorder)
    private buystockordersRepository: Repository<Buystockorder>,
    @InjectRepository(BuystockorderItem)
    private buystockorderItemsRepository: Repository<BuystockorderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Stock)
    private buystocksRepository: Repository<Stock>,
  ) {}
  async create(createBuystockOrderDto: CreateBuystockOrderDto) {
    const buystockOrder = new Buystockorder();
    const user = await this.usersRepository.findOneBy({
      id: createBuystockOrderDto.userId,
    });
    buystockOrder.user = user;
    buystockOrder.total = 0;
    buystockOrder.qty = 0;
    buystockOrder.buystockorderItems = [];
    for (const oi of createBuystockOrderDto.buystockOrderItems) {
      const buystockOrderItem = new BuystockorderItem();
      buystockOrderItem.stock = await this.buystocksRepository.findOneBy({
        id: oi.productId,
      });
      const stock = buystockOrderItem.stock; //
      stock.balance = stock.balance + oi.qty; //
      buystockOrderItem.name = buystockOrderItem.stock.name;
      buystockOrderItem.price = buystockOrderItem.stock.price;
      buystockOrderItem.qty = oi.qty;
      buystockOrderItem.unit = buystockOrderItem.stock.unit;
      buystockOrderItem.total = buystockOrderItem.price * buystockOrderItem.qty;
      await this.buystockorderItemsRepository.save(buystockOrderItem);
      buystockOrder.buystockorderItems.push(buystockOrderItem);
      buystockOrder.total += buystockOrderItem.total;
      buystockOrder.qty += buystockOrderItem.qty;
      this.buystocksRepository.save(stock); //
    }
    return this.buystockordersRepository.save(buystockOrder);
  }

  findAll() {
    return this.buystockordersRepository.find({
      relations: { buystockorderItems: true, user: true },
      order: {
        id: 'DESC',
      },
    });
  }

  findOne(id: number) {
    return this.buystockordersRepository.findOneOrFail({
      where: { id },
      relations: { buystockorderItems: true, user: true },
    });
  }

  update(id: number, UpdateBuystockOrderDto: UpdateBuystockOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.buystockordersRepository.findOneOrFail({
      where: { id },
    });
    await this.buystockordersRepository.remove(deleteOrder);

    return deleteOrder;
  }

  async findViewOrderBuyStockAvg() {
    const results = await this.buystocksRepository.query(
      'SELECT * FROM `viewBuyStockGraph1`',
    );
    return results;
  }

  async findViewOrderBuyStockDD() {
    const results = await this.buystocksRepository.query(
      'SELECT * FROM `viewBuyStockGraph2`',
    );
    return results;
  }

  async findViewOrderBuyStockYY() {
    const results = await this.buystocksRepository.query(
      'SELECT * FROM `viewBuyStockGraph3`',
    );
    return results;
  }
}
