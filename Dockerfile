FROM node:20.10.0

WORKDIR /user/src/app

COPY . .

RUN npm install

RUN npm install uuidv4 typeorm multer mysql2 --save

CMD ["npm", "run", "start:debug"]